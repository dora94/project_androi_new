<%@ include file="./header.jsp" %>

       
        
        <p>
            This was passed in from the controller thus showing that 
            the controller was accessed before the page was rendered. 
            This is MVC (Model View Controller) in action.
        </p>
        <p>
            Simple values can be rendered as so \${} i.e. here's the value from the controller: <blockquote>${hello}</blockquote>
        </p>
        <a href="viewdemo">PathVariable Demo</a><br/>
        <a href="paramdemo">RequestParam Demo</a>
        
<%@ include file="./footer.jsp" %>
