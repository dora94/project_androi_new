<%@ include file="./header.jsp" %>


        <p>normal parameter: <blockquote>${helloAgain}</blockquote></p>
        <h3>Person</h3>
        <p>Name: ${personObject.name}</p>
        <p>Age: ${personObject.age}</p>
        
        <p>
            <a href="<%=request.getContextPath()%>/person/Mary">Show Mary</a>
        </p>
        <p>
            <a href="<%=request.getContextPath()%>/person/Jane/28">Show Jane age 28</a>
        </p>
        <p>
            <a href="<%=request.getContextPath()%>/viewdemo">Show Default</a> (Jack, hardcoded in the controller)
        </p>
<%@ include file="./footer.jsp" %>