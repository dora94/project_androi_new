<%@ include file="./header.jsp" %>

<div align="center">
    <h2>Spring MVC Form Validation Demo - Login Form</h2>

    <span>email: admin@gmail.com password: abc123 </span><br/>
    <span>email: guest@gmail.com password: abc123 </span><br/>

    <table border="0" width="90%">
        <form:form action="login" modelAttribute="userForm" method="POST" >
            <tr>
                <td align="left" width="20%">Email: </td>
                <td align="left" width="40%"><form:input path="email" size="30"/></td>
                <td align="left"><form:errors path="email" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><form:password path="password" size="30"/></td>
                <td><form:errors path="password" cssClass="error"/></td>
            </tr>
            <tr>
                <td> </td>
                <td>
                    <form:select  path="kindOfUser">
                        <form:options items="${kind_lst}" />
                        </form:select>
                        </td>
                        <td><form:errors path="kindOfUser" cssClass="error"/></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center"><input type="submit" value="Login"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
            </div>

            <%@ include file="./footer.jsp" %>
