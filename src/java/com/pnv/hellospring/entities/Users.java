/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.hellospring.entities;


import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Administrator
 */
public class Users {
    @NotEmpty(message = "Please enter your email.")
    @Email
    private String email;
    
    @NotEmpty(message = "Please enter your password.")
    @Size(min = 3, max = 15, message = "Your password must between 3 and 15 characters")
    private String password;
    
     @NotEmpty(message = "Please choose kind of user")
    private String kindOfUser;

    public String getKindOfUser() {
        return kindOfUser;
    }

    public void setKindOfUser(String kindOfUser) {
        this.kindOfUser = kindOfUser;
    }

 

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

}
