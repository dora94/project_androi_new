package com.pnv.hellospring.validations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.pnv.hellospring.entities.Users;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Administrator
 */
public class UsersValidation implements Validator {

    @Override
    public boolean supports(Class<?> type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void validate(Object target, Errors errors) {
        Users user = (Users) target;
       
        if ( ! "abc123".equals( user.getPassword())) {
            errors.rejectValue("password", "user.pass.wrong");
        }
    }

}
